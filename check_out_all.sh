#!/bin/bash

if [ $# -lt 2 ] ; then
    echo Usage: $(basename $0) project version
    exit 1
fi

script_dir=$(cd $(dirname $0); pwd)

date

project=$1
PROJECT=$(echo $1 | tr a-z A-Z)
version=$2
projects=$(cd $LHCBRELEASES/$PROJECT/${PROJECT}_$version; cmt show projects | sed 's/(in.*//' | grep -v LCGCMT | awk '{if ($2) {print $2}}')

echo 'all:' > Makefile

for pv in $projects ; do
    p=$(echo $pv | cut -d_ -f1)
    v=$(echo $pv | cut -d_ -f2)
    if [ ! -e $p/$pv ] ; then
	echo Checking out $p $v
	getpack -Pr --batch $p $v
    fi
    if grep -q ^${pv}: Makefile ; then
	# echo ${pv} already inserted
	true
    else
	echo "" >> Makefile
	echo ${pv}: $(awk '/^use/{print $3}' $p/$pv/cmt/project.cmt | grep -v LCGCMT ) >> Makefile
	#echo $'\tcd '$p/$pv' ; cmt show projects'  >> Makefile
	echo $'\tcd '$p/$pv' ; cmt show projects' >> Makefile
	echo $'\t${MAKE} -C '$p/$pv >> Makefile
	echo all: ${pv} >> Makefile
    fi
done

# Patch GaudiPolicy ##########################################################
if [ -e GAUDI ] ; then
    gaudi_dir=GAUDI/$(ls GAUDI | tail -1)
    ( cd $gaudi_dir ; patch -p2 < $script_dir/GaudiPolicy.patch )
fi
# End of patch ###############################################################

#date
#CMTEXTRATAGS=profile-generate make -j 16

date
