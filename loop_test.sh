#!/bin/bash

script_dir=$(cd $(dirname $0); pwd)
project=DaVinci
version=v28r4p2
tar=${project}_${version}

for i in `seq 5` ; do
echo ============================================================================
echo Iteration $i optimized
date
echo ============================================================================
mkdir -p output
(. SetupProject.sh --dev-dir $script_dir/$tar.build.optimized $project $version --use-grid ; cd output ; time gaudirun.py -T ../options/DV16.py ../options/Reco09-Stripping13_SDSTs.py)
rm -rf output
echo ============================================================================
echo Iteration $i released
date
echo ============================================================================
mkdir -p output
(. SetupProject.sh $project $version --use-grid ; cd output ; time gaudirun.py -T ../options/DV16.py ../options/Reco09-Stripping13_SDSTs.py)
rm -rf output
done
