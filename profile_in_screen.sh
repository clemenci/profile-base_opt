#!/bin/bash

script_dir=$(cd $(dirname $0); pwd)

(screen -ls ; true) | grep -q profile && echo screen session already present && exit

screen -S profile -d -m
rm -f profile.screenlog
screen -S profile -p 0 -X logfile profile.screenlog
screen -S profile -p 0 -X log on
if [ $script_dir != $(pwd) ] ; then
    screen -S profile -X register c "cd $script_dir"$'\n'
    screen -S profile -p 0 -X paste c
fi
screen -S profile -X register c "./profile_DV_v28r4p2.sh"$'\n'
screen -S profile -p 0 -X paste c
screen -S profile -X screen top

(sleep 2 ; screen -S profile -X split ; screen -S profile -X focus down ; screen -S profile -X select 0)&

screen -r
