#!/bin/bash

script_dir=$(cd $(dirname $0); pwd)
project=DaVinci
version=v28r4p2
tar=${project}_${version}

function log() {
    echo $(date -Is): "$*"
}

# Remove old build directory
[ -e build ] && (mv build .build.removed && rm -rf .build.removed &)

log Prepare sources
if [ ! -e ${tar}.src.tar.bz2 ] ; then
    # Checkout the sources
    log Check out from repository
    mkdir -p build
    (cd build ; $script_dir/check_out_all.sh $project $version)

    # Keep a back up of the sources
    log Build tar file
    tar -c -j -f ${tar}.src.tar.bz2 build
else
    log Extract from tar file
    tar -x -j -f ${tar}.src.tar.bz2
fi

# Build to generate the profile data
log Build binaries for profiling
CMTEXTRATAGS=profile-generate,no-pyzip CMTPROJECTPATH=$PWD/build:$LHCBPROJECTPATH make -C build -j 16

log Back up and remove the .gcda files from the build
find build -name "*.gcda" | tar -c -v -j -f ${tar}.profile_build-time.tar.bz2 -T - | xargs rm

log Profiling the workload
mkdir -p output
(. SetupProject.sh --dev-dir $PWD/build $project $version --use-grid ; cd output ; time gaudirun.py -T ../options/DV16.py ../options/Reco09-Stripping13_SDSTs.py)
rm -rf output

log Collect .gcda data
find build -name "*.gcda" | tar -c -j -f ${tar}.profile_data.tar.bz2 -T -

log Prepare sources for new build  
mv -v build ${tar}.build.profile
tar -x -j -f ${tar}.src.tar.bz2
tar -x -j -f ${tar}.profile_data.tar.bz2

log Build optimized binaries
CMTEXTRATAGS=profile-use,no-pyzip CMTPROJECTPATH=$PWD/build:$LHCBPROJECTPATH make -C build -j 16

log Run optimized binaries
mkdir -p output
(. SetupProject.sh --dev-dir $PWD/build $project $version --use-grid ; cd output ; time gaudirun.py -T ../options/DV16.py ../options/Reco09-Stripping13_SDSTs.py)
rm -rf output

mv -v build ${tar}.build.optimized

log Run released version
mkdir -p output
(. SetupProject.sh $project $version --use-grid ; cd output ; time gaudirun.py -T ../options/DV16.py ../options/Reco09-Stripping13_SDSTs.py)
rm -rf output

log Done
